class BasePlugin {
    static getUrlPatterns() { return /.*/; }

    constructor(tabId, matchInfo) {
        this.tabId = tabId;
        this.matchInfo = matchInfo;
        this.key = matchInfo.pluginKey;
        this.pagePort = null;
        this.popupPort = null;
        this.pluginStatus = this.getInitialStatus();
    }

    init() {
        this.injectPageScript();
    }

    getInitialStatus() { return null; }
    getPageScriptPath() { return null; }
    getActions() { return {}; };

    injectPageScript() {
        const scriptPath = this.getPageScriptPath();
        if (!scriptPath) return;

        this.setUpPageListener();
        chrome.tabs.executeScript(this.tabId, { file: scriptPath }, () => {
            if (chrome.runtime.lastError) console.error(chrome.runtime.lastError);
        });
    }

    setUpPageListener() {
        chrome.runtime.onConnect.addListener((port) => {
            if (port.name !== this.key) return;
            if (port.sender.tab.id !== this.tabId) return;

            this.pagePort = port;
            this.pagePort.onMessage.addListener((data) => {
                this.handlePageMessage(data);
            });
            this.pagePort.onDisconnect.addListener(() => {
                this.pagePort = null;
            });
        });
    }

    sendMessageToPage(data) {
        if (!this.pagePort) return;
        this.pagePort.postMessage(data);
    }

    handlePageMessage(data) {
        if (data.status) this.updateStatus(data.status);
    }

    updateStatus(newStatus) {
        this.pluginStatus = newStatus;
        this.updatePopup();
    }

    updatePopup() {
        this.sendMessageToPopup({
            actions: this.getActions(),
            status: this.pluginStatus
        });
    }

    sendMessageToPopup(data) {
        if (!this.popupPort) return;
        this.popupPort.postMessage(data);
    }

    delay(msMin, msMax = null) {
        return new Promise((resolve, reject) => {
            let ms = msMin;
            if (msMax) ms = Math.floor(Math.random() * (msMax - msMin)) + msMin;

            setTimeout(resolve, ms);
        });
    }
}

const plugins = new Map();
