class Sample extends BasePlugin {
    static getUrlPatterns() {
        return [ /^https:\/\/frinkiac\.com\// ];
    }

    getPageScriptPath() {
        return 'plugins/sample/page.js';
    }
    getInitialStatus() {
        return 'Initial status';
    }
    getActions() {
        return { sampleAction: 'Sample Action' };
    }

    init() {
        this.injectPageScript();
        console.log('Injected page script');
    }

    async sampleAction() {
        console.log('Running sample action');
        this.sendMessageToPage({ action: 'Sample Action' });
    }
}

plugins.set('sample', Sample);
