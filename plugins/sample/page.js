console.log('Page script loaded');

const extPort = chrome.runtime.connect({ name: 'sample' });
extPort.postMessage({ status: 'Status update from page script' });
extPort.onMessage.addListener((data) => {
    console.log('Received message from extension:');
    console.log(data);
    extPort.postMessage({ status: 'Page script acknowledges action: ' + data.action });
});
