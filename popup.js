(() => {
    let numActions = 0;
    let status = null;
    hideSeparator();
    showFallbackMsg();

    window.port = chrome.runtime.connect({ name: 'extPopup' });

    window.port.onMessage.addListener((data) => {
        if (data.hasOwnProperty('actions')) {
            numActions = Object.keys(data.actions).length;
            updateActions(data.actions);
        }
        if (data.hasOwnProperty('status')) {
            status = data.status;
            updateStatus(status);
        }

        if (numActions < 1 || !status) hideSeparator();
        else showSeparator();

        if (numActions < 1 && !status) showFallbackMsg();
        else hideFallbackMsg();
    });
})();

function updateActions(actions) {
    let parent = document.getElementById('actions');
    parent.innerHTML = '';

    for (let [key, label] of Object.entries(actions)) {
        let a = document.createElement('a');

        a.innerHTML = label;
        a.setAttribute('href', '#');
        a.addEventListener('click', (evt) => {
            window.port.postMessage({ run: key });
        });

        parent.appendChild(a);
    }
}

function updateStatus(status) {
    let parent = document.getElementById('status');
    parent.innerHTML = status;
}

function showSeparator() {
    let separator = document.getElementById('separator');
    separator.setAttribute('style', 'display:block');
}

function hideSeparator() {
    let separator = document.getElementById('separator');
    separator.setAttribute('style', 'display:none');
}

function showFallbackMsg() {
    let msg = document.getElementById('fallback-msg');
    msg.setAttribute('style', 'display:block');
}

function hideFallbackMsg() {
    let msg = document.getElementById('fallback-msg');
    msg.setAttribute('style', 'display:none');
}
