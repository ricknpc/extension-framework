// -- basic setup -- //

const requestFilters = {
    urls: ['*://*/*'],
    types: ['main_frame']
};

const tabsCache = {};
const contentCache = {};

const iconData = {
    normal: {},
    dull: {}
};

for (let size of [16, 19, 32, 38, 48, 64, 128]) {
    size = size.toString();

    iconData.normal[size] = 'images/icon-' + size + '.png';
    iconData.dull[size] = 'images/icon-dull-' + size + '.png';
}
let activeTab = null;

// -- handle tabs changing, loading, becoming active, etc -- //

// when a tab is first loaded or gets a new url, check it for plugin match and initiate plugin
function initiatePluginForTab(tabId, url) {
    if (!tabId) return;

    let matchInfo = {
        pluginKey: null,
        pluginClass: null,
        patternIndex: null,
        matchObj: null
    };

    for (let [key, pluginClass] of plugins.entries()) {
        let patterns = pluginClass.getUrlPatterns();
        if (!Array.isArray(patterns)) patterns = [patterns];

        for (let [i, pattern] of patterns.entries()) {
            matchInfo.matchObj = url.match(pattern);
            if (matchInfo.matchObj) {
                matchInfo.pluginKey = key;
                matchInfo.pluginClass = pluginClass;
                matchInfo.patternIndex = i;
                break;
            }
        }
        if (matchInfo.matchObj) break;
    }

    if (matchInfo.pluginClass) {
        let plugin = new matchInfo.pluginClass(tabId, matchInfo);
        plugin.init();

        tabsCache[tabId] = plugin;
    } else {
        tabsCache[tabId] = false;
    }
}

// change icon based on whether the tab has a plugin or not
function tabActiveHandler(tabId = null) {
    activeTab = tabId;

    if (tabId && tabsCache[tabId]) chrome.browserAction.setIcon({ path: iconData.normal });
    else chrome.browserAction.setIcon({ path: iconData.dull });
}
tabActiveHandler();

// initiate when a new url is loaded
function webRequestHandler(details) {
    initiatePluginForTab(details.tabId, details.url);

    chrome.tabs.get(details.tabId, (tab) => {
        if (!tab.active) return;

        chrome.windows.get(tab.windowId, (window) => {
            if (window.focused) tabActiveHandler(details.tabId);
        });
    });
}
chrome.webRequest.onCompleted.addListener(webRequestHandler, requestFilters);

// when a tab is activated, if it doesn't have a cache initiate it
function tabChangeHandler(info) {
    if (tabsCache.hasOwnProperty(info.tabId)) return tabActiveHandler(info.tabId);

    chrome.tabs.get(info.tabId, (tab) => {
        initiatePluginForTab(tab.id, tab.url);
        tabActiveHandler(info.tabId);
    });
}
chrome.tabs.onActivated.addListener(tabChangeHandler);

// when a tab is updated, if the url changed initiate it
function tabUpdateHandler(tabId, changeInfo, tab) {
    if (changeInfo.url) initiatePluginForTab(tabId, changeInfo.url);
    if (tab.active) tabActiveHandler(tabId);
}
chrome.tabs.onUpdated.addListener(tabUpdateHandler);

// when a window is focused, set the active tab
function windowFocusHandler(windowId) {
    if (windowId === chrome.windows.WINDOW_ID_NONE) return;

    chrome.windows.get(windowId, { populate: true }, (window) => {
        if (!window) return;

        for (let tab of window.tabs) {
            if (tab.active) {
                tabChangeHandler({ tabId: tab.id });
                break;
            }
        }
    });
}
chrome.windows.onFocusChanged.addListener(windowFocusHandler);

// facilitate messages to and from popup
function popupConnectionHandler(port) {
    if (port.name !== 'extPopup') return;
    if (!tabsCache[activeTab]) return;

    let plugin = tabsCache[activeTab];

    plugin.popupPort = port;
    port.onDisconnect.addListener(() => {
        plugin.popupPort = null;
    });
    plugin.updatePopup();

    port.onMessage.addListener((data) => {
        if (data.run) plugin[data.run]();
    });
}
chrome.runtime.onConnect.addListener(popupConnectionHandler);
