#!/usr/bin/env bash

zip -r extension-framework.xpi ./* -x .sass-cache -x .git -x .gitignore -x pack-xpi.sh
